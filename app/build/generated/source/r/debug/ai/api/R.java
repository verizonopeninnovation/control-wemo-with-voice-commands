/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package ai.api;

public final class R {
	public static final class attr {
		public static final int centerColor = 0x7f010042;
		public static final int circleCenterX = 0x7f010040;
		public static final int circleCenterY = 0x7f010041;
		public static final int haloColor = 0x7f010043;
		public static final int mainColor = 0x7f01002b;
		public static final int maxRadius = 0x7f01003f;
		public static final int minRadius = 0x7f01003e;
		public static final int state_listening = 0x7f010044;
		public static final int state_speaking = 0x7f010046;
		public static final int state_waiting = 0x7f010045;
	}
	public static final class color {
		public static final int aidialog_background = 0x7f0a0006;
		public static final int icon_orange_color = 0x7f0a001e;
		public static final int main_bg_color = 0x7f0a0021;
		public static final int mic_button_halo = 0x7f0a0027;
		public static final int mic_colors = 0x7f0a0043;
	}
	public static final class dimen {
		public static final int mic_button_halo_radius_max = 0x7f080032;
		public static final int mic_button_halo_radius_min = 0x7f080033;
		public static final int mic_button_size = 0x7f080034;
	}
	public static final class drawable {
		public static final int cube = 0x7f020037;
		public static final int mic_control = 0x7f020039;
		public static final int microphone_control = 0x7f02003a;
	}
	public static final class id {
		public static final int micButton = 0x7f0b0047;
		public static final int micContainer = 0x7f0b0046;
		public static final int partialResultsTextView = 0x7f0b0049;
		public static final int titleTextView = 0x7f0b0048;
	}
	public static final class layout {
		public static final int aidialog = 0x7f030017;
	}
	public static final class string {
		public static final int app_name = 0x7f050010;
		public static final int default_aidialog_title = 0x7f050011;
	}
	public static final class style {
		public static final int ApiAi = 0x7f06006c;
		public static final int ApiAi_FullScreenDialog = 0x7f06006d;
		public static final int ApiAi_Microphone = 0x7f06006e;
	}
	public static final class styleable {
		public static final int[] MaskedColorView = { 0x7f01002b };
		public static final int MaskedColorView_mainColor = 0;
		public static final int[] SoundLevelButton = { 0x7f01003e, 0x7f01003f, 0x7f010040, 0x7f010041, 0x7f010042, 0x7f010043, 0x7f010044, 0x7f010045, 0x7f010046 };
		public static final int SoundLevelButton_centerColor = 4;
		public static final int SoundLevelButton_circleCenterX = 2;
		public static final int SoundLevelButton_circleCenterY = 3;
		public static final int SoundLevelButton_haloColor = 5;
		public static final int SoundLevelButton_maxRadius = 1;
		public static final int SoundLevelButton_minRadius = 0;
		public static final int SoundLevelButton_state_listening = 6;
		public static final int SoundLevelButton_state_speaking = 8;
		public static final int SoundLevelButton_state_waiting = 7;
	}
}
